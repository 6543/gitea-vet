## [0.2.0](https://gitea.com/gitea/gitea-vet/pulls?q=&type=all&state=closed&milestone=1272) - 2020-07-20

* FEATURES
  * Add migrations check (#5)
* BUGFIXES
  * Correct Import Paths (#6)
